/*	############################################################
	# @(#) chr_iso.h
	# @(#)
	# @(#) Copyright (c) 1995-2001 by Dirk Hagedorn
	# @(#) Dirk Hagedorn (udo@dirk-hagedorn.de)
	#
	# This program is free software; you can redistribute it and/or
	# modify it under the terms of the GNU General Public License
	# as published by the Free Software Foundation; either version 2
	# of the License, or (at your option) any later version.
	# 
	# This program is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU General Public License for more details.
	# 
	# You should have received a copy of the GNU General Public License
	# along with this program; if not, write to the Free Software
	# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	# 
	# @(#) Uebersetzungstabellen fuer Commodore Amiga und Linux
	# Werte laut Screenshot von Michael Gehrke @ MK2
	# und Tabelle von Frank Baumgart @ PB3
	############################################################	*/

#include	"portab.h"

#ifndef ID_CHARSET_H
#define	ID_CHARSET_H
const char *id_charset_h= "@(#) chr_iso.h   16.07.1998";
#endif

#ifndef	THIS_CHARSET
#define	THIS_CHARSET	"ISO Latin1"
#endif


/*	------------------------------------------------------------	*/

/*	############################################################
	# ISO-Zeichensatz in System-Zeichensatz umsetzen
	#
	# Bei ANSI und ISO stimmen die Zeichen von \240 bis \377
	# ueberein, dennoch wird diese Tabelle mitgeschleppt, damit
	# in chars.c nicht unnoetige Abfragen erfolgen muessen.
	############################################################	*/

typedef struct _iso2sys
{	unsigned char	iso;
	unsigned char	sys;
}	ISO2SYS;

LOCAL const ISO2SYS iso2sys_item[128]=
{
	{ 128, 0   },
	{ 129, 0   },
	{ 130, 0   },
	{ 131, 0   },
	{ 132, 0   },
	{ 133, 0   },
	{ 134, 0   },
	{ 135, 0   },
	{ 136, 0   },
	{ 137, 0   },
	{ 138, 0   },
	{ 139, 0   },
	{ 140, 0   },
	{ 141, 0   },
	{ 142, 0   },
	{ 143, 0   },
	{ 144, 0   },
	{ 145, 0   },
	{ 146, 0   },
	{ 147, 0   },
	{ 148, 0   },
	{ 149, 0   },
	{ 150, 0   },
	{ 151, 0   },
	{ 152, 0   },
	{ 153, 0   },
	{ 154, 0   },
	{ 155, 0   },
	{ 156, 0   },
	{ 157, 0   },
	{ 158, 0   },
	{ 159, 0   },
	{ 160, 160 },
	{ 161, 161 },
	{ 162, 162 },
	{ 163, 163 },
	{ 164, 164 },
	{ 165, 165 },
	{ 166, 166 },
	{ 167, 167 },
	{ 168, 168 },
	{ 169, 169 },
	{ 170, 170 },
	{ 171, 171 },
	{ 172, 172 },
	{ 173, 173 },
	{ 174, 174 },
	{ 175, 175 },
	{ 176, 176 },
	{ 177, 177 },
	{ 178, 178 },
	{ 179, 179 },
	{ 180, 180 },
	{ 181, 181 },
	{ 182, 182 },
	{ 183, 183 },
	{ 184, 184 },
	{ 185, 185 },
	{ 186, 186 },
	{ 187, 187 },
	{ 188, 188 },
	{ 189, 189 },
	{ 190, 190 },
	{ 191, 191 },
	{ 192, 192 },
	{ 193, 193 },
	{ 194, 194 },
	{ 195, 195 },
	{ 196, 196 },
	{ 197, 197 },
	{ 198, 198 },
	{ 199, 199 },
	{ 200, 200 },
	{ 201, 201 },
	{ 202, 202 },
	{ 203, 203 },
	{ 204, 204 },
	{ 205, 205 },
	{ 206, 206 },
	{ 207, 207 },
	{ 208, 208 },
	{ 209, 209 },
	{ 210, 210 },
	{ 211, 211 },
	{ 212, 212 },
	{ 213, 213 },
	{ 214, 214 },
	{ 215, 215 },
	{ 216, 216 },
	{ 217, 217 },
	{ 218, 218 },
	{ 219, 219 },
	{ 220, 220 },
	{ 221, 221 },
	{ 222, 222 },
	{ 223, 223 },
	{ 224, 224 },
	{ 225, 225 },
	{ 226, 226 },
	{ 227, 227 },
	{ 228, 228 },
	{ 229, 229 },
	{ 230, 230 },
	{ 231, 231 },
	{ 232, 232 },
	{ 233, 233 },
	{ 234, 234 },
	{ 235, 235 },
	{ 236, 236 },
	{ 237, 237 },
	{ 238, 238 },
	{ 239, 239 },
	{ 240, 240 },
	{ 241, 241 },
	{ 242, 242 },
	{ 243, 243 },
	{ 244, 244 },
	{ 245, 245 },
	{ 246, 246 },
	{ 247, 247 },
	{ 248, 248 },
	{ 249, 249 },
	{ 250, 250 },
	{ 251, 251 },
	{ 252, 252 },
	{ 253, 253 },
	{ 254, 254 },
	{ 255, 255 },
};


/*	------------------------------------------------------------	*/

/*	############################################################
	# Systemzeichensatz in andere Zeichensaetze umwandeln
	############################################################	*/

typedef struct _chartable
{
	unsigned char	system;
	char ascii[16];
	char ansi[16];
	char tex[16];
	char html[16];
	char ps[6];
}	CHARTABLE;

LOCAL /*const*/ CHARTABLE chrtab[128]=
{
	{	128, "",	"",			"",					"&#128;",	"\\200"	},
	{	129, "",	"",			"",					"&#129;",	"\\201"	},
	{	130, "",	"",			"",					"&#130;",	"\\202"	},
	{	131, "",	"",			"",					"&#131;",	"\\203"	},
	{	132, "",	"",			"",					"&#132;",	"\\204"	},
	{	133, "",	"",			"",					"&#133;",	"\\205"	},
	{	134, "",	"",			"",					"&#134;",	"\\206"	},
	{	135, "",	"",			"",					"&#135;",	"\\207"	},
	{	136, "",	"",			"",					"&#136;",	"\\210"	},
	{	137, "",	"",			"",					"&#137;",	"\\211"	},
	{	138, "",	"",			"",					"&#138;",	"\\212"	},
	{	139, "",	"",			"",					"&#139;",	"\\213"	},
	{	140, "",	"",			"",					"&#140;",	"\\214"	},
	{	141, "",	"",			"",					"&#141;",	"\\215"	},
	{	142, "",	"",			"",					"&#142;",	"\\216"	},
	{	143, "",	"",			"",					"&#143;",	"\\217"	},
	{	144, "",	"",			"",					"&#144;",	"\\220"	},
	{	145, "",	"",			"",					"&#145;",	"\\221"	},
	{	146, "",	"",			"",					"&#146;",	"\\222"	},
	{	147, "",	"",			"",					"&#147;",	"\\223"	},
	{	148, "",	"",			"",					"&#148;",	"\\224"	},
	{	149, "",	"",			"",					"&#149;",	"\\225"	},
	{	150, "",	"",			"",					"&#150;",	"\\226"	},
	{	151, "",	"",			"",					"&#151;",	"\\227"	},
	{	152, "",	"",			"",					"&#152;",	"\\230"	},
	{	153, "",	"",			"",					"&#153;",	"\\231"	},
	{	154, "",	"",			"",					"&#154;",	"\\232"	},
	{	155, "",	"",			"",					"&#155;",	"\\233"	},
	{	156, "",	"",			"",					"&#156;",	"\\234"	},
	{	157, "",	"",			"",					"&#157;",	"\\235"	},
	{	158, "",	"",			"",					"&#158;",	"\\236"	},
	{	159, "",	"",			"",					"&#159;",	"\\237"	},
	{	160, "",	"\\'A0",	"",					"&#160;",	"\\240"	},
	{	161, "!",	"\\'A1",	"!`",				"&#161;",	"\\241"	},
	{	162, "",	"\\'A2",	"cent",				"&#162;",	"\\242"	},
	{	163, "",	"\\'A3",	"\\pounds{}",		"&#163;",	"\\243"	},
	{	164, "",	"\\'A4",	"",					"&#164;",	"\\244"	},	/* general-currency sign */
	{	165, "",	"\\'A5",	"yen",				"&#165;",	"\\245"	},
	{	166, "",	"\\'A6",	"$\\mid$",			"&#166;",	"\\246"	},	/* broken-vertical-line */
	{	167, "",	"\\'A7",	"\\S{}",			"&sect;",	"\\247"	},
	{	168, "",	"\\'A8",	"\\\"{ }",			"&#168;",	"\\250"	},
	{	169, "(C)",	"\\'A9",	"\\copyright{}",	"&copy;",	"\\251"	},
	{	170, "",	"\\'AA",	"\\b{a}",			"&#170;",	"\\252"	},
	{	171, "<",	"\\'AB",	"\"<",				"&#171;",	"\\253"	},
	{	172, "",	"\\'AC",	"$\\neq$",			"&#172;",	"\\254"	},
	{	173, "",	"\\'AD",	"\\-",				"&#173;",	"\\255"	},
	{	174, "(R)",	"\\'AE",	"(R)",				"&#174;",	"\\256"	},
	{	175, "",	"\\'AF",	"",					"&#175;",	"\\257"	},	/* macron */
	{	176, "",	"\\'B0",	"$\\circ$",			"&#176;",	"\\260"	},
	{	177, "",	"\\'B1",	"$\\pm$",			"&#177;",	"\\261"	},
	{	178, "",	"\\'B2",	"$^2$",				"&#178;",	"\\262"	},
	{	179, "",	"\\'B3",	"$^3$",				"&#179;",	"\\263"	},
	{	180, "",	"\\'B4",	"\\'{ }",			"&#180;",	"\\264"	},
	{	181, "",	"\\'B5",	"$\\mu$",			"&#181;",	"\\265"	},
	{	182, "",	"\\'B6",	"\\P{}",			"&#182;",	"\\266"	},
	{	183, "",	"\\'B7",	"\\cdot{}",			"&#183;",	"\\267"	},
	{	184, "",	"\\'B8",	"\\c{ }",			"&#184;",	"\\270"	},
	{	185, "",	"\\'B9",	"$^1$",				"&#185;",	"\\271"	},
	{	186, "",	"\\'BA",	"\\b{o}",			"&#186;",	"\\272"	},
	{	187, ">",	"\\'BB",	"\">",				"&#187;",	"\\273"	},
	{	188, "1/4",	"\\'BC",	"$\\frac{1}{4}$",	"&#188;",	"\\274"	},
	{	189, "1/2",	"\\'BD",	"$\\frac{1}{2}$",	"&#189;",	"\\275"	},
	{	190, "3/4",	"\\'BE",	"$\\frac{3}{4}$",	"&#180;",	"\\276"	},
	{	191, "?",	"\\'BF",	"?`",				"&#191;",	"\\277"	},
	{	192, "A",	"\\'C0",	"\\`{A}",			"&Agrave;",	"\\300"	},
	{	193, "A",	"\\'C1",	"\\'{A}",			"&Aacute;",	"\\301"	},
	{	194, "A",	"\\'C2",	"\\^{A}",			"&Acirc;",	"\\302"	},
	{	195, "A",	"\\'C3",	"\\~{A}",			"&Atilde;",	"\\303"	},
	{	196, "Ae",	"\\'C4",	"{\\\"A}",			"&Auml;",	"\\304"	},
	{	197, "A",	"\\'C5",	"{\\AA}",			"&Aring;",	"\\305"	},
	{	198, "AE",	"\\'C6",	"{\\AE}",			"&AElig;",	"\\306"	},
	{	199, "C",	"\\'C7",	"\\c{C}",			"&Ccedil;",	"\\307"	},
	{	200, "E",	"\\'C8",	"\\`{E}",			"&Egrave;",	"\\310"	},
	{	201, "E",	"\\'C9",	"\\'{E}",			"&Eacute;",	"\\311"	},
	{	202, "E",	"\\'CA",	"\\^{E}",			"&Ecirc;",	"\\312"	},
	{	203, "E",	"\\'CB",	"\\\"E",			"&Euml;",	"\\313"	},
	{	204, "I",	"\\'CC",	"\\`{I}",			"&Igrave;",	"\\314"	},
	{	205, "I",	"\\'CD",	"\\'{I}",			"&Iacute;",	"\\315"	},
	{	206, "I",	"\\'CE",	"\\^{I}",			"&Icirc;",	"\\316"	},
	{	207, "I",	"\\'CF",	"\\\"I",			"&Iuml;",	"\\317"	},
	{	208, "N",	"\\'D0",	"",					"&#208;",	"\\320"	},	/* -D */
	{	209, "O",	"\\'D1",	"\\~{N}",			"&Ntilde;",	"\\321"	},
	{	210, "O",	"\\'D2",	"\\`{O}",			"&Ograve;",	"\\322"	},
	{	211, "O",	"\\'D3",	"\\'{O}",			"&Oacute;",	"\\323"	},
	{	212, "O",	"\\'D4",	"\\^{O}",			"&Ocirc;",	"\\324"	},
	{	213, "O",	"\\'D5",	"\\~{O}",			"&Otilde;",	"\\325"	},
	{	214, "Oe",	"\\'D6",	"{\\\"O}",			"&Ouml;",	"\\326"	},
	{	215, "x",	"\\'D7",	"$\\times$",		"&#215;",	"\\327"	},
	{	216, "O",	"\\'D8",	"{\\O}",			"&Oslash;",	"\\330"	},
	{	217, "U",	"\\'D9",	"\\`{U}",			"&Ugrave;",	"\\331"	},
	{	218, "U",	"\\'DA",	"\\'{U}",			"&Uacute;",	"\\332"	},
	{	219, "U",	"\\'DB",	"\\^{U}",			"&Ucirc;",	"\\333"	},
	{	220, "Ue",	"\\'DC",	"{\\\"U}",			"&Uuml;",	"\\334"	},
	{	221, "Y",	"\\'DD",	"\\'{Y}",			"&Yacute;",	"\\335"	},
	{	222, "",	"\\'DE",	"",					"&#222;",	"\\336"	},	/* |o */
	{	223, "ss",	"\\'DF",	"{\\ss}",			"&szlig;",	"\\337"	},
	{	224, "a",	"\\'E0",	"\\`{a}",			"&agrave;",	"\\340"	},
	{	225, "a",	"\\'E1",	"\\'{a}",			"&aacute;",	"\\341"	},
	{	226, "a",	"\\'E2",	"\\^{a}",			"&acirc;",	"\\342"	},
	{	227, "a",	"\\'E3",	"\\~{a}",			"&atilde;",	"\\343"	},
	{	228, "ae",	"\\'E4",	"{\\\"a}",			"&auml;",	"\\344"	},
	{	229, "a",	"\\'E5",	"{\\aa}",			"&aring;",	"\\345"	},
	{	230, "ae",	"\\'E6",	"{\\ae}",			"&aelig;",	"\\346"	},
	{	231, "c",	"\\'E7",	"\\c{c}",			"&ccedil;",	"\\347"	},
	{	232, "e",	"\\'E8",	"\\`{e}",			"&egrave;",	"\\350"	},
	{	233, "e",	"\\'E9",	"\\'{e}",			"&eacute;",	"\\351"	},
	{	234, "e",	"\\'EA",	"\\^{e}",			"&ecirc;",	"\\352"	},
	{	235, "e",	"\\'EB",	"\\\"e",			"&euml;",	"\\353"	},
	{	236, "i",	"\\'EC",	"\\`{i}",			"&igrave;",	"\\354"	},
	{	237, "i",	"\\'ED",	"\\'{i}",			"&iacute;",	"\\355"	},
	{	238, "i",	"\\'EE",	"\\^{i}",			"&icirc;",	"\\356"	},
	{	239, "i",	"\\'EF",	"\\\"{i}",			"&iuml;",	"\\357"	},
	{	240, "",	"\\'F0",	"",					"&#240;",	"\\360"	},	/* THORN */
	{	241, "n",	"\\'F1",	"\\~{n}",			"&ntilde;",	"\\361"	},
	{	242, "o",	"\\'F2",	"\\`{o}",			"&ograve;",	"\\362"	},
	{	243, "o",	"\\'F3",	"\\'{o}",			"&oacute;",	"\\363"	},
	{	244, "o",	"\\'F4",	"\\^{o}",			"&ocirc;",	"\\364"	},
	{	245, "o",	"\\'F5",	"\\~{o}",			"&otilde;",	"\\365"	},
	{	246, "oe",	"\\'F6",	"{\\\"o}",			"&ouml;",	"\\366"	},
	{	247, "",	"\\'F7",	"$\\div$",			"&#247;",	"\\367"	},
	{	248, "o",	"\\'F8",	"{\\o}",			"&oslash;",	"\\370"	},
	{	249, "u",	"\\'F9",	"\\`{u}",			"&ugrave;",	"\\371"	},
	{	250, "u",	"\\'FA",	"\\'{u}",			"&uacute;",	"\\372"	},
	{	251, "u",	"\\'FB",	"\\^{u}",			"&ucirc;",	"\\373"	},
	{	252, "ue",	"\\'FC",	"{\\\"u}",			"&uuml;",	"\\374"	},
	{	253, "y",	"\\'FD",	"\\'{y}",			"&yacute;",	"\\375"	},
	{	254, "",	"\\'FE",	"",					"&#254;",	"\\376"	},	/* |B */
	{	255, "y",	"\\'FF",	"\\\"{y}",			"&yuml;",	"\\377"	}
};

/*	------------------------------------------------------------	*/

/*	############################################################
	# universellen Zeichensatz in ASCII-Zeichensatz umsetzen
	############################################################	*/
typedef struct _uni2systab
{
	char			uni[8];
	unsigned char	system[2];
}	UNI2SYSTAB;

#define	UNI2SYSTABSIZE	64

LOCAL const UNI2SYSTAB uni2sys[UNI2SYSTABSIZE]=
{
	{	"(!\"a)",	"\344"	},
	{	"(!\"e)",	"\353"	},
	{	"(!\"i)",	"\357"	},
	{	"(!\"o)",	"\366"	},
	{	"(!\"u)",	"\374"	},
	{	"(!\"y)",	"\377"	},
	{	"(!\"s)",	"\337"	},
	{	"(!\"A)",	"\304"	},
	{	"(!\"E)",	"\313"	},
	{	"(!\"I)",	"\317"	},
	{	"(!\"O)",	"\326"	},
	{	"(!\"U)",	"\334"	},
	{	"(!\"Y)",	"\237"	},	/* 13 */

	{	"(!'a)",	"\341"	},
	{	"(!'e)",	"\351"	},
	{	"(!'i)",	"\355"	},
	{	"(!'o)",	"\363"	},
	{	"(!'u)",	"\372"	},
	{	"(!'y)",	"\375"	},
	{	"(!'A)",	"\301"	},
	{	"(!'E)",	"\311"	},
	{	"(!'I)",	"\315"	},
	{	"(!'O)",	"\323"	},
	{	"(!'U)",	"\332"	},
	{	"(!'Y)",	"\335"	},	/* 12 */

	{	"(!`a)",	"\340"	},
	{	"(!`e)",	"\350"	},
	{	"(!`i)",	"\354"	},
	{	"(!`o)",	"\362"	},
	{	"(!`u)",	"\371"	},
	{	"(!`A)",	"\300"	},
	{	"(!`E)",	"\310"	},
	{	"(!`I)",	"\314"	},
	{	"(!`O)",	"\322"	},
	{	"(!`U)",	"\331"	},	/* 10 */

	{	"(!^a)",	"\342"	},
	{	"(!^e)",	"\352"	},
	{	"(!^i)",	"\356"	},
	{	"(!^o)",	"\364"	},
	{	"(!^u)",	"\373"	},
	{	"(!^A)",	"\302"	},
	{	"(!^E)",	"\312"	},
	{	"(!^I)",	"\316"	},
	{	"(!^O)",	"\324"	},
	{	"(!^U)",	"\333"	},	/* 10 */

	{	"(!~a)",	"\343"	},
	{	"(!~n)",	"\361"	},
	{	"(!~o)",	"\365"	},
	{	"(!~A)",	"\303"	},
	{	"(!~N)",	"\321"	},
	{	"(!~O)",	"\325"	},	/*  6 */

	{	"(!.a)",	"\345"	},
	{	"(!.A)",	"\305"	},	/*  2 */

	{	"(!&ae)",	"\346"	},
	{	"(!&AE)",	"\306"	},
	{	"(!&oe)",	"\234"	},
	{	"(!&OE)",	"\214"	},	/*  4 */

	{	"(!,c)",	"\347"	},
	{	"(!,C)",	"\307"	},	/*  2 */

	{	"(!_a)",	"\252"	},
	{	"(!_o)",	"\272"	},	/*  2 */

	{	"(!\\o)",	"\370"	},
	{	"(!\\O)",	"\330"	},	/*  2 */

	{	"(!#S)",	"\247"	},
};


/*	############################################################
	# chr_iso.h
	############################################################	*/

