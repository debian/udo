/*	############################################################
	# @(#) chr_hp8.h
	# @(#)
	# @(#) Copyright (c) 1995-2001 by Dirk Hagedorn
	# @(#) Dirk Hagedorn (udo@dirk-hagedorn.de)
	#
	# This program is free software; you can redistribute it and/or
	# modify it under the terms of the GNU General Public License
	# as published by the Free Software Foundation; either version 2
	# of the License, or (at your option) any later version.
	# 
	# This program is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU General Public License for more details.
	# 
	# You should have received a copy of the GNU General Public License
	# along with this program; if not, write to the Free Software
	# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	# 
	# @(#) Uebersetzungstabellen fuer HP-UX
	# @(#) Charset: HP-Roman-8
	############################################################	*/

#ifndef ID_CHARSET_H
#define	ID_CHARSET_H
const char *id_charset_h= "@(#) chr_hp8.h   18.04.1996";
#endif

#ifndef	THIS_CHARSET
#define	THIS_CHARSET	"HP Roman 8"
#endif


/*	------------------------------------------------------------	*/

/*	############################################################
	# ISO-Zeichensatz in System-Zeichensatz umsetzen
	############################################################	*/

typedef struct _iso2sys
{	unsigned char iso;
	char sys;
}	ISO2SYS;

LOCAL const ISO2SYS iso2sys_item[128]=
{
	{ 128, 0      },		/* null		*/
	{ 129, 0      },		/* null		*/
	{ 130, 0      },		/* '' (9u)	*/
	{ 131, '\276' },		/* #f		*/
	{ 132, 0      },		/* "" (99u)	*/
	{ 133, 0      },		/* !..		*/
	{ 134, 0      },		/* #dag		*/
	{ 135, 0      },		/* 2x#dag	*/
	{ 136, 0      },		/* #circ	*/
	{ 137, 0      },		/* #promill	*/
	{ 138, '\353' },		/* vS		*/
	{ 139, 0      },		/* <		*/
	{ 140, 0      },		/* &OE		*/
	{ 141, 0      },		/* null		*/
	{ 142, 0      },		/* null		*/
	{ 143, 0      },		/* null		*/
	{ 144, 0      },		/* null		*/
	{ 145, 0      },		/* '' (6o)	*/
	{ 146, 0      },		/* '' (9o)	*/
	{ 147, 0      },		/* "" (66o)	*/
	{ 148, 0      },		/* "" (99u)	*/
	{ 149, '\362' },		/* #bullet	*/
	{ 150, 0      },		/* --		*/
	{ 151, 0      },		/* ---		*/
	{ 152, '\254' },		/* !~		*/
	{ 153, 0      },		/* #tm		*/
	{ 154, '\354' },		/* vs		*/
	{ 155, 0      },		/* >		*/
	{ 156, 0      },		/* &oe		*/
	{ 157, 0      },		/* null		*/
	{ 158, 0      },		/* null		*/
	{ 159, '\356' },		/* "Y		*/
	{ 160, 0      },		/* null		*/
	{ 161, '\270' },		/* #!		*/
	{ 162, '\277' },		/* #cent	*/
	{ 163, '\273' },		/* #pound	*/
	{ 164, '\272' },		/* general currency	*/
	{ 165, '\274' },		/* #yen		*/
	{ 166, 0      },		/* broken dash	*/
	{ 167, '\275' },		/* #sect	*/
	{ 168, '\253' },		/* #"		*/
	{ 169, 0      },		/* #copy	*/
	{ 170, '\371' },		/* _a		*/
	{ 171, '\373' },		/* #<		*/
	{ 172, 0      },		/* #neg		*/
	{ 173, 0      },		/* strich	*/
	{ 174, 0      },		/* #reg		*/
	{ 175, '\260' },		/* upline	*/
	{ 176, '\263' },		/* #degree	*/
	{ 177, '\376' },		/* #pm		*/
	{ 178, 0      },		/* #^2		*/
	{ 179, 0      },		/* #^3		*/
	{ 180, '\250' },		/* #'		*/
	{ 181, '\363' },		/* #mu		*/
	{ 182, '\364' },		/* #p		*/
	{ 183, 0      },		/* #cdot	*/
	{ 184, 0      },		/* cedille	*/
	{ 185, 0      },		/* #^1		*/
	{ 186, '\372' },		/* _o		*/
	{ 187, '\375' },		/* #>		*/
	{ 188, '\367' },		/* #1/4		*/
	{ 189, '\370' },		/* #1/2		*/
	{ 190, '\365' },		/* #3/4		*/
	{ 191, '\271' },		/* #?		*/
	{ 192, '\241' },		/* `A		*/
	{ 193, '\340' },		/* 'A		*/
	{ 194, '\242' },		/* ^A		*/
	{ 195, '\341' },		/* ~A		*/
	{ 196, '\330' },		/* "A		*/
	{ 197, '\320' },		/* .A		*/
	{ 198, '\323' },		/* &AE		*/
	{ 199, '\264' },		/* ,C		*/
	{ 200, '\243' },		/* `E		*/
	{ 201, '\334' },		/* 'E		*/
	{ 202, '\244' },		/* ^E		*/
	{ 203, '\245' },		/* "E		*/
	{ 204, '\346' },		/* `I		*/
	{ 205, '\345' },		/* 'I		*/
	{ 206, '\246' },		/* ^I		*/
	{ 207, '\247' },		/* "I		*/
	{ 208, '\343' },		/* -D		*/
	{ 209, '\266' },		/* ~N		*/
	{ 210, '\350' },		/* `O		*/
	{ 211, '\347' },		/* 'O		*/
	{ 212, '\337' },		/* ^O		*/
	{ 213, '\351' },		/* ~O		*/
	{ 214, '\332' },		/* "O		*/
	{ 215, 0      },		/* #times	*/
	{ 216, '\322' },		/* /O		*/
	{ 217, '\255' },		/* `U		*/
	{ 218, '\355' },		/* 'U		*/
	{ 219, '\256' },		/* ^U		*/
	{ 220, '\333' },		/* "U		*/
	{ 221, '\261' },		/* 'Y		*/
	{ 222, '\360' },		/* |b		*/
	{ 223, '\336' },		/* "s		*/
	{ 224, '\310' },		/* `a		*/
	{ 225, '\304' },		/* 'a		*/
	{ 226, '\300' },		/* ^a		*/
	{ 227, '\342' },		/* ~a		*/
	{ 228, '\314' },		/* "a		*/
	{ 229, '\324' },		/* .a		*/
	{ 230, '\327' },		/* &ae		*/
	{ 231, '\265' },		/* ,c		*/
	{ 232, '\311' },		/* `e		*/
	{ 233, '\305' },		/* 'e		*/
	{ 234, '\301' },		/* ^e		*/
	{ 235, '\315' },		/* "e		*/
	{ 236, '\331' },		/* `i		*/
	{ 237, '\325' },		/* 'i		*/
	{ 238, '\321' },		/* ^i		*/
	{ 239, '\335' },		/* "i		*/
	{ 240, '\344' },		/* o|''		*/
	{ 241, '\267' },		/* ~n		*/
	{ 242, '\312' },		/* `o		*/
	{ 243, '\306' },		/* 'o		*/
	{ 244, '\302' },		/* ^o		*/
	{ 245, '\352' },		/* ~o		*/
	{ 246, '\316' },		/* "o		*/
	{ 247, 0      },		/* #div		*/
	{ 248, '\326' },		/* /o		*/
	{ 249, '\313' },		/* `u		*/
	{ 250, '\307' },		/* 'u		*/
	{ 251, '\303' },		/* ^u		*/
	{ 252, '\317' },		/* "u		*/
	{ 253, '\262' },		/* 'y		*/
	{ 254, '\361' },		/* |B		*/
	{ 255, '\357' },		/* "y		*/
};

/*	------------------------------------------------------------	*/

/*	############################################################
	# Systemzeichensatz in andere Zeichensaetze umwandeln
	############################################################	*/

typedef struct _chartable
{
	unsigned char system;
	char ascii[16];
	char ansi[16];
	char tex[16];
	char html[16];
}	CHARTABLE;

LOCAL /* const */ CHARTABLE chrtab[128]=
{
	{	128,	"",		"",			"",					"&#128;"	},
	{	129,	"",		"",			"",					"&#129;"	},
	{	130,	"",		"",			"",					"&#130;"	},
	{	131,	"",		"",			"",					"&#131;"	},
	{	132,	"",		"",			"",					"&#132;"	},
	{	133,	"",		"",			"",					"&#133;"	},
	{	134,	"",		"",			"",					"&#134;"	},
	{	135,	"",		"",			"",					"&#135;"	},
	{	136,	"",		"",			"",					"&#136;"	},
	{	137,	"",		"",			"",					"&#137;"	},
	{	138,	"",		"",			"",					"&#138;"	},
	{	139,	"",		"",			"",					"&#139;"	},
	{	140,	"",		"",			"",					"&#140;"	},
	{	141,	"",		"",			"",					"&#141;"	},
	{	142,	"",		"",			"",					"&#142;"	},
	{	143,	"",		"",			"",					"&#143;"	},
	{	144,	"",		"",			"",					"&#144;"	},
	{	145,	"",		"",			"",					"&#145;"	},
	{	146,	"",		"",			"",					"&#146;"	},
	{	147,	"",		"",			"",					"&#147;"	},
	{	148,	"",		"",			"",					"&#148;"	},
	{	149,	"",		"",			"",					"&#149;"	},
	{	150,	"",		"",			"",					"&#150;"	},
	{	151,	"",		"",			"",					"&#151;"	},
	{	152,	"",		"",			"",					"&#152;"	},
	{	153,	"",		"",			"",					"&#153;"	},
	{	154,	"",		"",			"",					"&#154;"	},
	{	155,	"",		"",			"",					"&#155;"	},
	{	156,	"",		"",			"",					"&#156;"	},
	{	157,	"",		"",			"",					"&#157;"	},
	{	158,	"",		"",			"",					"&#158;"	},
	{	159,	"",		"",			"",					"&#159;"	},
	{	160,	"",		"",			"",					"&#160;"	},
	{	161,	"A",	"\\'C0",	"\\`{A}",			"&Agrave;"	},
	{	162,	"A",	"\\'C2",	"\\^{A}",			"&Acirc;"	},
	{	163,	"E",	"\\'C8",	"\\'{E}",			"&Egrave;"	},
	{	164,	"E",	"\\'CA",	"\\^{E}",			"&Ecirc;"	},
	{	165,	"E",	"\\'CB",	"\\\"{E}",			"&Euml;"	},
	{	166,	"I",	"\\'CE",	"\\^{I}",			"&Icirc;"	},
	{	167,	"I",	"\\'CF",	"\\\"{I}",			"&Iuml;"	},
	{	168,	"'",	"\\'B4",	"'",				"'"			},	/* #' */
	{	169,	"`",	"`",		"`",				"`"			},	/* `  */
	{	170,	"",		"\\~",		"~",				"&nbsp;"	},	/* ~  */
	{	171,	"\"",	"\\'A8",	"\\\"{ }",			"\""		},
	{	172,	"~",	"\\'98",	"$\\symbol{94}$",	"~"			},	/* !~ */
	{	173,	"U",	"\\'D9",	"\\`{U}",			"&Ugrave;"	},
	{	174,	"U",	"\\'DB",	"\\^{U}",			"&Ucirc;"	},
	{	175,	"",		"",			"",					"&#163;"	},	/* dp */
	{	176,	"",		"\\'AF",	"",					"&#175;"	},	/* macron	*/
	{	177,	"Y",	"\\'DD",	"\\'{Y}",			"&Yacute;"	},
	{	178,	"y",	"\\'FD",	"\\'{y}",			"&yacute;"	},
	{	179,	"",		"\\'B0",	"$\\circ$",			"&#176;"	},
	{	180,	"C",	"\\'C7",	"\\c{C}",			"&Ccedil;"	},
	{	181,	"c",	"\\'E7",	"\\c{c}",			"&ccedil;"	},
	{	182,	"N",	"\\'D1",	"\\~{N}",			"&Ntilde;"	},
	{	183,	"n",	"\\'F1",	"\\~{n}",			"&ntilde;"	},
	{	184,	"!",	"\\'A1",	"!`",				"&#161;"	},
	{	185,	"?",	"\\'BF",	"?`",				"&#191;"	},
	{	186,	"",		"\\'A4",	"",					""			},	/* gcs	*/
	{	187,	"",		"\\'A3",	"\\pounds{}",		""			},
	{	188,	"",		"\\'A5",	"yen",				""			},
	{	189,	"",		"\\'A7",	"\\S{}",			""			},
	{	190,	"",		"\\'83",	"$f$",				""			},
	{	191,	"",		"\\'A2",	"cent",				""			},
	{	192,	"a",	"\\'E2",	"\\^{a}",			"&acirc;"	},
	{	193,	"e",	"\\'EA",	"\\^{e}",			"&ecirc;"	},
	{	194,	"o",	"\\'F4",	"\\^{o}",			"&ocirc;"	},
	{	195,	"u",	"\\'FB",	"\\^{u}",			"&ucirc;"	},
	{	196,	"a",	"\\'E1",	"\\'{a}",			"&aacute;"	},
	{	197,	"e",	"\\'E9",	"\\'{e}",			"&eacute;"	},
	{	198,	"o",	"\\'F3",	"\\'{o}",			"&oacute;"	},
	{	199,	"u",	"\\'FA",	"\\'{u}",			"&uacute;"	},
	{	200,	"a",	"\\'E0",	"\\`{a}",			"&agrave;"	},
	{	201,	"e",	"\\'E8",	"\\`{e}",			"&egrave;"	},
	{	202,	"o",	"\\'F2",	"\\`{o}",			"&ograve;"	},
	{	203,	"u",	"\\'F9",	"\\`{u}",			"&ugrave;"	},
	{	204,	"ae",	"\\'E4",	"{\\\"a}",			"&auml;"	},
	{	205,	"e",	"\\'EB",	"\\\"{e}",			"&euml;"	},
	{	206,	"oe",	"\\'F6",	"{\\\"o}",			"&ouml;"	},
	{	207,	"ue",	"\\'FC",	"{\\\"u}",			"&uuml;"	},
	{	208,	"A",	"\\'C5",	"{\\AA}",			"&Aring;"	},
	{	209,	"i",	"\\'EE",	"\\^{i}",			"&icirc;"	},
	{	210,	"O",	"\\'D8",	"{\\O}",			"&Oslash;"	},
	{	211,	"AE",	"\\'C6",	"{\\AE}",			"&AElig;"	},
	{	212,	"a",	"\\'E5",	"{\\aa}",			"&aring;"	},
	{	213,	"i",	"\\'ED",	"\\'{i}",			"&iacute;"	},
	{	214,	"o",	"\\'F8",	"{\\o}",			"&oslash;"	},
	{	215,	"ae",	"\\'E6",	"{\\ae}",			"&aelig;"	},
	{	216,	"Ae",	"\\'C4",	"{\\\"A}",			"&Auml;"	},
	{	217,	"i",	"\\'EC",	"\\`{i}",			"&igrave;"	},
	{	218,	"Oe",	"\\'D6",	"{\\\"O}",			"&Ouml;"	},
	{	219,	"Ue",	"\\'DC",	"{\\\"U}",			"&Uuml;"	},
	{	220,	"E",	"\\'C9",	"\\`{E}",			"&Eacute;"	},
	{	221,	"i",	"\\'EF",	"\\\"{i}",			"&iuml;"	},
	{	222,	"ss",	"\\'DF",	"{\\ss}",			"&szlig;"	},
	{	223,	"O",	"\\'D4",	"\\^{O}",			"&Ocirc;"	},
	{	224,	"A",	"\\'C1",	"\\'{A}",			"&Aacute;"	},
	{	225,	"A",	"\\'C3",	"\\~{A}",			"&Atilde;"	},
	{	226,	"a",	"\\'E3",	"\\~{a}",			"&atilde;"	},
	{	227,	"",		"\\'D0",	"",					"&#208;"	},	/* -D */
	{	228,	"",		"\\'F0",	"",					""			},	/* o|''	*/
	{	229,	"I",	"\\'CD",	"\\'{I}",			"&Iacute;"	},
	{	230,	"I",	"\\'CC",	"\\`{I}",			"&Igrave;"	},
	{	231,	"O",	"\\'D3",	"\\'{O}",			"&Oacute;"	},
	{	232,	"O",	"\\'D2",	"\\`{O}",			"&Ograve;"	},
	{	233,	"O",	"\\'D5",	"\\~{O}",			"&Otilde;"	},
	{	234,	"o",	"\\'F5",	"\\~{o}",			"&otilde;"	},
	{	235,	"S",	"\\'8A",	"\\v{S}",			"&#138;"	},
	{	236,	"s",	"\\'9A",	"\\v{s}",			"&#154;"	},
	{	237,	"U",	"\\'DA",	"\\'{U}",			"&Uacute;"	},
	{	238,	"Y",	"\\'9F",	"\\\"{Y}",			"&Yuml;"	},
	{	239,	"y",	"\\'FF",	"\\\"{y}",			"&yuml;"	},
	{	240,	"",		"\\'DE",	"",					""			},	/* |b */
	{	241,	"",		"\\'FE",	"",					""			},	/* |B */
	{	242,	"*",	"\\'95",	"$\\bullet$",		"&#149;"	},
	{	243,	"",		"\\'B5",	"$\\mu$",			"&#181;"	},
	{	244,	"",		"\\'B6",	"\\P{}",			"&#182;"	},
	{	245,	"3/4",	"\\'BE",	"$\\frac{3}{4}$",	"&#190;"	},
	{	246,	"--",	"-",		"--",				"&#150;"	},
	{	247,	"1/4",	"\\'BC",	"$\\frac{1}{4}$",	"&#188;"	},
	{	248,	"1/2",	"\\'BD",	"$\\frac{1}{2}$",	"&#189;"	},
	{	249,	"",		"\\'AA",	"\\b{a}",			"&#170;"	},
	{	250,	"",		"\\'BA",	"\\b{o}",			"&#186;"	},
	{	251,	"<",	"\\'AB",	"\"<",				"&#171;"	},
	{	252,	"*",	"\\'95",	"$\\bullet$",		"&#149;"	},
	{	253,	">",	"\\'BB",	"\">",				"&#187;"	},
	{	254,	"",		"\\'B1",	"$\\pm$",			"&#177;"	},
	{	255,	"",		"",			"",					""			}
};

/*	------------------------------------------------------------	*/

/*	############################################################
	# universellen Zeichensatz in ASCII-Zeichensatz umsetzen
	############################################################	*/
typedef struct _uni2systab
{
	char	uni[8];
	char	system[2];
}	UNI2SYSTAB;

#define	UNI2SYSTABSIZE	64

LOCAL const UNI2SYSTAB uni2sys[UNI2SYSTABSIZE]=
{
	{	"(!\"a)",	"\314"	},
	{	"(!\"e)",	"\315"	},
	{	"(!\"i)",	"\335"	},
	{	"(!\"o)",	"\316"	},
	{	"(!\"u)",	"\317"	},
	{	"(!\"y)",	"\357"	},
	{	"(!\"s)",	"\336"	},
	{	"(!\"A)",	"\330"	},
	{	"(!\"E)",	"\245"	},
	{	"(!\"I)",	"\247"	},
	{	"(!\"O)",	"\332"	},
	{	"(!\"U)",	"\333"	},
	{	"(!\"Y)",	"\356"	},	/* 13 */

	{	"(!'a)",	"\304"	},
	{	"(!'e)",	"\305"	},
	{	"(!'i)",	"\325"	},
	{	"(!'o)",	"\306"	},
	{	"(!'u)",	"\307"	},
	{	"(!'y)",	"\262"	},
	{	"(!'A)",	"\340"	},
	{	"(!'E)",	"\334"	},
	{	"(!'I)",	"\346"	},
	{	"(!'O)",	"\347"	},
	{	"(!'U)",	"\355"	},
	{	"(!'Y)",	"\261"	},	/* 12 */

	{	"(!`a)",	"\310"	},
	{	"(!`e)",	"\311"	},
	{	"(!`i)",	"\331"	},
	{	"(!`o)",	"\312"	},
	{	"(!`u)",	"\313"	},
	{	"(!`A)",	"\241"	},
	{	"(!`E)",	"\243"	},
	{	"(!`I)",	"\345"	},
	{	"(!`O)",	"\350"	},
	{	"(!`U)",	"\255"	},	/* 10 */

	{	"(!^a)",	"\300"	},
	{	"(!^e)",	"\301"	},
	{	"(!^i)",	"\321"	},
	{	"(!^o)",	"\302"	},
	{	"(!^u)",	"\303"	},
	{	"(!^A)",	"\242"	},
	{	"(!^E)",	"\244"	},
	{	"(!^I)",	"\245"	},
	{	"(!^O)",	"\337"	},
	{	"(!^U)",	"\256"	},	/* 10 */

	{	"(!~a)",	"\342"	},
	{	"(!~n)",	"\267"	},
	{	"(!~o)",	"\352"	},
	{	"(!~A)",	"\341"	},
	{	"(!~N)",	"\266"	},
	{	"(!~O)",	"\351"	},	/*  6 */

	{	"(!.a)",	"\324"	},
	{	"(!.A)",	"\320"	},	/*  2 */

	{	"(!&ae)",	"\327"	},
	{	"(!&AE)",	"\323"	},
	{	"(!&oe)",	""		},
	{	"(!&OE)",	""		},	/*  4 */

	{	"(!,c)",	"\265"	},
	{	"(!,C)",	"\264"	},	/*  2 */

	{	"(!_a)",	"\371"	},
	{	"(!_o)",	"\372"	},	/*  2 */

	{	"(!\\o)",	"\326"	},
	{	"(!\\O)",	"\322"	},	/*  2 */

	{	"(!#S)",	"\275"	},
};

/*	############################################################
	# chr_hp8.h
	############################################################	*/
